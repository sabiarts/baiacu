﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    int score =0;
	int m_Score =0;
    public Text scoreText;
	public Text bestScoreText;
	public GameObject gameOver;
	public GameObject maxDestroyer;

	Vector2 destroyerPosition;


    void Start()
    {
		ResetScore ();
		destroyerPosition = maxDestroyer.transform.position;
		SetText ();
		bestScoreText.text = "Best Score: " + PlayerPrefs.GetInt("Best Score").ToString();
		//PlayerPrefs.DeleteAll ();

    }

    public void ScoreUp()
	{
        score ++;
        scoreText.text = "Score: " + score.ToString();
    }

	public void GameOver ()
	{
		gameOver.SetActive (true);
		Time.timeScale = 0;
		maxDestroyer.transform.position = new Vector2 (0, 0);
		if (score > m_Score) 
		{
			PlayerPrefs.SetInt ("Best Score", score);
		}
	}

	public void Resume(){
		gameOver.SetActive (false);
		Time.timeScale = 1;
		ResetScore ();
		StartCoroutine (KillAll ());
	}

	void ResetScore()
	{
		score = 0;
		scoreText.text = "Score: 0";
		bestScoreText.text = "Best Score: " + PlayerPrefs.GetInt("Best Score").ToString();
	}

	void SetText()
	{
		//Fetch the score from the PlayerPrefs (set these Playerprefs in another script). If no Int of this name exists, the default is 0.
		m_Score = PlayerPrefs.GetInt("Best Score");
	}
		
	IEnumerator KillAll(){
		yield return new WaitForFixedUpdate();
		maxDestroyer.transform.position = destroyerPosition;
	}
}
