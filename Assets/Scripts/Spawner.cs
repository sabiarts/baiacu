﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject carrot;
    public GameObject knife;


    public float spawnMargin;
    public float spawnRatio;
    public float killRatio;
    

    GameObject[] spawnItems = new GameObject[4];

    //GameMaster gameMaster;
    //Checkpoint firstCheckpoint;


    void Start()
    {
        spawnItems = new GameObject[] {carrot, knife};
        //gameMaster = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameMaster>();
        //firstCheckpoint = GetComponent<Checkpoint>();
        StartCoroutine(Spawn());
        //Debug.Log(gameMaster);
    }

    IEnumerator Spawn(){
        Vector2 spawnPoint;
        int spawnId = 0;
        int goodOrBad = 0;
        while (true){
            yield return new WaitForSeconds(spawnRatio);
            goodOrBad = Random.Range(0, 100);
            if (goodOrBad < killRatio){
                spawnId = 0;
            } else{
                spawnId = 1;
            }
            spawnPoint = new Vector2(transform.position.x + Random.Range(spawnMargin * -1, spawnMargin), transform.position.y + Random.Range(spawnMargin * -1, spawnMargin));
            GameObject item = Instantiate(spawnItems[spawnId], spawnPoint, transform.rotation);
            //Physics2D.IgnoreCollision(GetComponent<Collider2D>(), cell.GetComponent<Collider2D>());
            //cell.GetComponent<Cell>().SetDirection(firstCheckpoint.Redirect());
            //gameMaster.allCells.Add(cell);




        }
    }
}

