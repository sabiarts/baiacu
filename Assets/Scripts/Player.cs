﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    public float moveSpeed;
    public GameControl gameControl;
	public AudioClip eatSound;
	public AudioClip hitSound;

	AudioSource audioSource;

	void Start(){
		audioSource = GetComponent<AudioSource>();
	}

    void FixedUpdate()
    {
        PlayerMove(moveSpeed * Time.deltaTime);
        
    }

    void PlayerMove(float speed){
        if(Input.touchCount > 0){
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[Input.touchCount - 1].position);
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, touchPosition.y), speed);
        }
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "ScoreUp"){
            gameControl.ScoreUp();
			audioSource.clip = eatSound;
			audioSource.Play ();
			Destroy (col.gameObject);
        }

        if (col.tag == "Kill"){
            Debug.Log("DED");
			Destroy (col.gameObject);
			audioSource.clip = hitSound;
			audioSource.Play ();
			gameControl.GameOver ();
        }
    }


}
