﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public float moveSpeed;
    Transform blackHole;
    
    void Start()
    {
        blackHole = GameObject.FindGameObjectWithTag("Destroyer").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(
            transform.position, new Vector2(blackHole.position.x, transform.position.y), moveSpeed * Time.deltaTime);
    }
}
